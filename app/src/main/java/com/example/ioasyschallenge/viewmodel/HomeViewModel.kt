package com.example.ioasyschallenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appservice.data.Enterprises
import com.example.appservice.repository.EnterpriseRepository
import com.example.ioasyschallenge.ui.adapter.HomeState
import com.example.ioasyschallenge.viewmodel.SubscriberSelector.applyStandardObservable

class HomeViewModel(
    private val repository: EnterpriseRepository
): ViewModel() {

    val homeState: MutableLiveData<HomeState> = MutableLiveData(HomeState.START)
    val homeStateLiveData: LiveData<HomeState> = homeState

    private val onSearchSuccess: MutableLiveData<Enterprises> = MutableLiveData()
    val onSearchSuccessLiveData: LiveData<Enterprises> = onSearchSuccess

    private val onSearchFailed: MutableLiveData<Throwable> = MutableLiveData()
    val onSearchFailedLiveData: LiveData<Throwable> = onSearchFailed

    fun postState(state: HomeState) {
        homeState.postValue(state)
    }

    fun showSearchInput() {
        postState(HomeState.HOME_SEARCH)
    }

    fun homeState() {
        postState(HomeState.START)
    }

    fun search(name: String) {
        repository.getEnterprises(name = name)
            .compose(applyStandardObservable())
            .subscribe(
                onSearchSuccess::postValue,
                onSearchFailed::postValue
            )
    }

}