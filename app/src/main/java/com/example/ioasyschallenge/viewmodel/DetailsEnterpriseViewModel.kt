package com.example.ioasyschallenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appservice.data.Enterprise

class DetailsEnterpriseViewModel: ViewModel() {

    var enterprise: MutableLiveData<Enterprise> = MutableLiveData()

    private val goBack: MutableLiveData<Unit> = MutableLiveData()
    val goBackLiveData: LiveData<Unit> = goBack

    fun postEnterprise(enterprise: Enterprise) {
        this.enterprise.postValue(enterprise)
    }

    fun back() { goBack.postValue(Unit) }

}