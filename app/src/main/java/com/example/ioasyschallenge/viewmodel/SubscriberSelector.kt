package com.example.ioasyschallenge.viewmodel

import rx.Observable
import rx.android.schedulers.AndroidSchedulers.mainThread
import rx.schedulers.Schedulers.io

object SubscriberSelector {

    fun <T> applyStandardObservable(): Observable.Transformer<T, T> =
        Observable.Transformer {
            it
                .subscribeOn(io())
                .observeOn(mainThread())
        }
}