package com.example.ioasyschallenge.viewmodel

import android.os.Parcelable
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.appservice.data.User
import com.example.appservice.repository.UserRepository
import com.example.ioasyschallenge.BuildConfig
import com.example.ioasyschallenge.util.Logger
import com.example.ioasyschallenge.viewmodel.SubscriberSelector.applyStandardObservable
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
class LoginViewModel(
    private val repository: UserRepository
): ViewModel(), Parcelable {

    val username: MutableLiveData<String> = MutableLiveData()
    val password: MutableLiveData<String> = MutableLiveData()

    private val fieldsInvalid: MutableLiveData<Boolean> = MutableLiveData()
    val fieldsInvalidLiveData: LiveData<Boolean> = fieldsInvalid

    private val onSuccessSignedIn: MutableLiveData<User> = MutableLiveData()
    val onSuccessSignedInLiveData: LiveData<User> = onSuccessSignedIn

    private val onErrorSigningIn: MutableLiveData<Throwable> = MutableLiveData()
    val onErrorSigningInLiveData: LiveData<Throwable> = onErrorSigningIn

    fun validateFields() {
        if (username.value.isNullOrEmpty() || password.value.isNullOrEmpty()) {
            fieldsInvalid.postValue(true)
        } else {
            fieldsInvalid.postValue(false)
            signIn()
        }
    }

    fun post() {
        username.postValue(BuildConfig.USERNAME)
        password.postValue(BuildConfig.PASS)
    }

    private fun signIn() {
        Logger.e("SIGNIN", "${username.value}, ${password.value}")
        username.value?.let { username ->
            password.value?.let { password ->

                repository.signIn(username, password)
                    .compose(applyStandardObservable())
                    .subscribe(
                        onSuccessSignedIn::postValue,
                        onErrorSigningIn::postValue
                    )

            } ?: run {
                fieldsInvalid.postValue(true)
            }
        } ?: run {
            fieldsInvalid.postValue(true)
        }

    }
}