package com.example.ioasyschallenge.util

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.ioasyschallenge.BuildConfig


object ImageDownloadService {

    fun downloadImage(
        imgName: String,
        img: ImageView,
        c: Context
    ) {

        Glide
            .with(c)
            .load(BuildConfig.BASE_URL + imgName)
            .diskCacheStrategy(DiskCacheStrategy.DATA)
            .fitCenter()
            .into(img)
    }
}