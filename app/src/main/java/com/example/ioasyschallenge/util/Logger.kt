package com.example.ioasyschallenge.util

import android.util.Log
import com.example.ioasyschallenge.BuildConfig

object Logger {

    const val TAG = "EMPRESAS_IOASYS"
    val log = BuildConfig.DEBUG

    fun d(tag: String = TAG, message: String) {
        if (log) Log.d(tag, message)
    }

    fun e(tag: String = TAG, message: String) {
        if (log) Log.e(tag, message)
    }
}