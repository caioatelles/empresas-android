package com.example.ioasyschallenge.di

import com.example.appservice.datasource.EnterpriseDataSource
import com.example.appservice.datasource.UserDataSource
import com.example.appservice.repository.EnterpriseRepository
import com.example.appservice.repository.UserRepository
import com.example.ioasyschallenge.viewmodel.DetailsEnterpriseViewModel
import com.example.ioasyschallenge.viewmodel.HomeViewModel
import com.example.ioasyschallenge.viewmodel.LoginViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * API Request Module - associated to the android module :appservice
 */
val serviceModule = module {
    factory { UserDataSource() }
    factory { EnterpriseDataSource() }

    single { UserRepository() }
    single { EnterpriseRepository() }
}

/**
 * View Model Module - associated to each Viewmodel on viewmodel package
 */
val vmmodules = module {
    viewModel { LoginViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { DetailsEnterpriseViewModel() }
}