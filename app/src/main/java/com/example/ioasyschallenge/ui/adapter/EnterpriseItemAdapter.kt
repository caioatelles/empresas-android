package com.example.ioasyschallenge.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.appservice.data.Enterprise
import com.example.ioasyschallenge.databinding.AdapterItemEnterpriseBinding
import com.example.ioasyschallenge.util.ImageDownloadService

class EnterpriseItemAdapter(
    private val items: MutableList<Enterprise>,
    private val clickListener: (item: Enterprise) -> Unit
): RecyclerView.Adapter<EnterpriseItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AdapterItemEnterpriseBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val enterprise = items[position]
        holder.bind(enterprise, clickListener)
    }

    override fun getItemId(position: Int) = items[position].id.toLong()

    override fun getItemCount() = items.size

    inner class ViewHolder(private val binding: AdapterItemEnterpriseBinding) :
        RecyclerView.ViewHolder(binding.root) {

            fun bind(item: Enterprise, listener: (item: Enterprise) -> Unit) {

                binding.apply {
                    this.item = item
                    root.setOnClickListener {
                        listener.invoke(item)
                    }
                }

            }

    }

}