package com.example.ioasyschallenge.ui

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.appservice.data.Enterprise
import com.example.appservice.data.Enterprises
import com.example.appservice.repository.EnterpriseRepository
import com.example.ioasyschallenge.R
import com.example.ioasyschallenge.databinding.FragmentHomeBinding
import com.example.ioasyschallenge.ui.DetailsEnterpriseFragment.Companion.ENTERPRISE_TAG
import com.example.ioasyschallenge.ui.adapter.EnterpriseItemAdapter
import com.example.ioasyschallenge.ui.adapter.HomeState
import com.example.ioasyschallenge.util.LoadController
import com.example.ioasyschallenge.util.Util.hideKeyboard
import com.example.ioasyschallenge.util.Util.paint
import com.example.ioasyschallenge.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.*

class HomeFragment : Fragment() {

    private val repository: EnterpriseRepository by inject()
    private val viewModel: HomeViewModel by viewModel {
        parametersOf(repository)
    }

    private lateinit var adapter: EnterpriseItemAdapter
    private val enterprises: MutableList<Enterprise> = mutableListOf()

    private lateinit var load: LoadController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(inflater).let {
        it.lifecycleOwner = viewLifecycleOwner
        it.viewModel = viewModel
        it.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSearchAction()
        configureObservers()
        configureRecycler()
        setStatusBarColor()
        setLoad()
    }

    private fun setStatusBarColor() {
        activity?.window?.paint(resources.getColor(R.color.medium_pink))
    }

    private fun setLoad() {
        load = LoadController(requireContext())
    }

    private fun setSearchAction() {
        edt_search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    load.showStandardLoading("")
                    viewModel.search(edt_search.text.toString())
                    activity?.hideKeyboard()
                    return true
                }
                return false
            }
        })
    }

    private fun configureRecycler() {
        adapter = EnterpriseItemAdapter(
            enterprises,
            ::onSelectEnterprise
        )
        adapter.setHasStableIds(true)

        rcv_enterprises.apply {

            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = this@HomeFragment.adapter

        }
    }

    private fun configureObservers() {

        viewModel.apply {

            onSearchFailedLiveData.observe(
                viewLifecycleOwner,
                ::doOnError
            )

            onSearchSuccessLiveData.observe(
                viewLifecycleOwner,
                ::doOnSuccess
            )

            homeStateLiveData.observe(
                viewLifecycleOwner,
                ::setHome
            )

        }

    }

    private fun doOnError(error: Throwable) {
        Log.e("OnError", error.toString())
        load.dismissIt()
        viewModel.postState(HomeState.HOME_NOT_FOUND)
    }

    private fun doOnSuccess(enterprises: Enterprises) {
        load.dismissIt()
        if (enterprises.enterprises.isNotEmpty()) {
            viewModel.postState(HomeState.HOME_FOUND)
            updateEnterprisesList(enterprises.enterprises)
        } else {
            edt_search.setText("")
            viewModel.postState(HomeState.HOME_NOT_FOUND)
        }
    }

    private fun updateEnterprisesList(enterprises: List<Enterprise>) {
        this.enterprises.clear()
        Collections.sort(enterprises, Comparator.comparing(Enterprise::enterpriseName))
        this.enterprises.addAll(enterprises)
        adapter.notifyDataSetChanged()
    }

    private fun onSelectEnterprise(e: Enterprise) {
        setInitialState()
        findNavController().navigate(
            R.id.action_homeFragment_to_detailsEnterpriseFragment,
            bundleOf(ENTERPRISE_TAG to e)
        )
    }

    private fun setInitialState() {
        viewModel.postState(HomeState.START)
        CoroutineScope(Main).launch {
            enterprises.clear()
            delay(500)
            adapter.notifyDataSetChanged()
        }
    }

    private fun setHome(state: HomeState) {
        if (state != HomeState.HOME_SEARCH) edt_search.setText("")
    }
}