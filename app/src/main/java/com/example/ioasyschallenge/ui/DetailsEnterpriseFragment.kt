package com.example.ioasyschallenge.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.appservice.data.Enterprise
import com.example.ioasyschallenge.R
import com.example.ioasyschallenge.databinding.FragmentDetailsEnterpriseBinding
import com.example.ioasyschallenge.viewmodel.DetailsEnterpriseViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main
import org.koin.android.viewmodel.ext.android.viewModel

class DetailsEnterpriseFragment : Fragment() {

    private val viewModel: DetailsEnterpriseViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailsEnterpriseBinding.inflate(inflater).let {
        it.lifecycleOwner = viewLifecycleOwner
        it.viewModel = viewModel
        it.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureObservers()
    }

    private fun configureObservers() {
        viewModel.apply {

            postEnterprise(arguments?.getSerializable(ENTERPRISE_TAG) as Enterprise)

            goBackLiveData.observe(
                viewLifecycleOwner,
                ::goBack
            )
        }
    }

    private fun goBack(u: Unit) {
        CoroutineScope(Main).launch {
            findNavController()
                .navigate(R.id.action_detailsEnterpriseFragment_to_homeFragment)

        }
    }

    companion object {
        const val ENTERPRISE_TAG = "ENTERPRISE_ON_BUNDLE"
    }
}