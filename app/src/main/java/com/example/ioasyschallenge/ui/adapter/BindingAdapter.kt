package com.example.ioasyschallenge.ui.adapter

import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.example.ioasyschallenge.util.ImageDownloadService

object BindingAdapter {

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun getImage(view: ImageView, url: String?) {
        url?.let {
            ImageDownloadService.downloadImage(
                it,
                view,
                view.context
            )
        }
    }

    @BindingAdapter("inputSearchVisibility")
    @JvmStatic
    fun searchHeader(view: View, state: HomeState) {
        view.visibility = if (state == HomeState.HOME_SEARCH)
            VISIBLE
        else GONE
    }

    @BindingAdapter("logoHeaderVisibility")
    @JvmStatic
    fun logoHeader(view: View, state: HomeState) {
        view.visibility = if (state != HomeState.HOME_SEARCH)
            VISIBLE
        else GONE
    }

    @BindingAdapter("searchFoundVisibility")
    @JvmStatic
    fun enterpriseFound(view: View, state: HomeState) {
        view.visibility = if (state == HomeState.HOME_FOUND)
            VISIBLE
        else GONE
    }

    @BindingAdapter("searchNotFoundVisibility")
    @JvmStatic
    fun enterpriseNotFound(view: View, state: HomeState) {
        view.visibility = if (state == HomeState.HOME_NOT_FOUND)
            VISIBLE
        else GONE
    }

    @BindingAdapter("startVisibility")
    @JvmStatic
    fun start(view: View, state: HomeState) {
        view.visibility = if (state == HomeState.START)
            VISIBLE
        else GONE
    }

    @BindingAdapter("itemTxt")
    fun text(view: TextView, txt: String) {
        view.text = txt
    }

}

enum class HomeState {
    START,
    HOME_SEARCH,
    HOME_FOUND,
    HOME_NOT_FOUND
}