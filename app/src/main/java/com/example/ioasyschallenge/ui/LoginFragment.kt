package com.example.ioasyschallenge.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.appservice.data.User
import com.example.appservice.repository.UserRepository
import com.example.ioasyschallenge.R
import com.example.ioasyschallenge.databinding.FragmentLoginBinding
import com.example.ioasyschallenge.util.LoadController
import com.example.ioasyschallenge.util.Logger
import com.example.ioasyschallenge.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.fragment_login.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class LoginFragment: Fragment() {

    private val repository: UserRepository by inject()
    private val viewModel: LoginViewModel by viewModel {
        parametersOf(repository)
    }

    private lateinit var load: LoadController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentLoginBinding.inflate(inflater).let {
        it.lifecycleOwner = viewLifecycleOwner
        it.viewModel = viewModel
        it.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configureObservers()
        setLoad()
    }

    private fun setLoad() {
        load = LoadController(requireContext())
    }

    private fun configureObservers() {
        viewModel.apply {

            fieldsInvalidLiveData.observe(
                viewLifecycleOwner,
                ::toggleFieldStatus
            )

            onSuccessSignedInLiveData.observe(
                viewLifecycleOwner,
                ::doOnSuccess
            )

            onErrorSigningInLiveData.observe(
                viewLifecycleOwner,
                ::doOnError
            )
        }
    }

    private fun doOnSuccess(user: User) {
        Logger.e("SIGNIN", "$user")
        toggleFieldStatus(false)
        load.dismissIt()
        findNavController().navigate(R.id.action_loginFragment_to_homeFragment)
    }

    private fun doOnError(error: Throwable) {
        Logger.e("SIGNIN", "$error")
        load.dismissIt()
        toggleFieldStatus(true)

    }

    private fun toggleFieldStatus(failed: Boolean) {
        if (failed) {
            edt_email.error = ""
            edt_password.error = ""
            txv_error.visibility = VISIBLE
        } else {
            load.showStandardLoading("")
            edt_email.error = null
            edt_password.error = null
            txv_error.visibility = GONE
        }
    }

}