package com.example.ioasyschallenge.app

import android.app.Application
import com.example.ioasyschallenge.di.serviceModule
import com.example.ioasyschallenge.di.vmmodules
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


class IoasysApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@IoasysApplication)
            modules(serviceModule, vmmodules)
        }

        Thread.setDefaultUncaughtExceptionHandler( ExceptionHandler() )

        AppCenter.start(
            this,
            "5baf1d87-c0da-4752-a984-044ae2085e03",
            Analytics::class.java,
            Crashes::class.java
        )
    }
}