package com.example.ioasyschallenge.app

import android.os.Process
import com.example.ioasyschallenge.BuildConfig
import com.microsoft.appcenter.crashes.Crashes
import java.io.PrintWriter
import java.io.StringWriter
import kotlin.system.exitProcess

class ExceptionHandler: Thread.UncaughtExceptionHandler {

    override fun uncaughtException(thread: Thread, exception: Throwable) {
        val stackTrace = StringWriter()
        exception.printStackTrace(PrintWriter(stackTrace))


        if (BuildConfig.ANALYTICS) { Crashes.trackError(exception) }

        System.err.println(stackTrace)
        Process.killProcess(Process.myPid())
        exitProcess(0)
    }
}