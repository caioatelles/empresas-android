package com.example.appservice.repository

import android.os.Parcel
import android.os.Parcelable
import com.example.appservice.BuildConfig
import com.example.appservice.data.Enterprise
import com.example.appservice.data.Enterprises
import com.example.appservice.datasource.EnterpriseDataSource
import kotlinx.android.parcel.Parcelize
import rx.Observable

@Parcelize
class EnterpriseRepository : Repository<Enterprise>(
    EnterpriseDataSource()
), Parcelable {

    fun getEnterprises(typeId: Int? = null, name: String? = null): Observable<Enterprises> =
        Observable.create {
            val call = (dataSource as EnterpriseDataSource).getEnterprises(typeId, name)
            callback(call, it)
        }

    fun getEnterprise(id: Int): Observable<Enterprise> =
        Observable.create {
            val call = (dataSource as EnterpriseDataSource).getEnterprise(id)
            callback(call, it)
        }

}