package com.example.appservice.repository

import android.os.Parcelable
import com.example.appservice.BuildConfig
import com.example.appservice.data.User
import com.example.appservice.data.UserWrapper
import com.example.appservice.datasource.UserDataSource
import kotlinx.android.parcel.Parcelize
import rx.Observable

@Parcelize
class UserRepository: Repository<User>(dataSource = UserDataSource()), Parcelable {


    fun signIn(username: String, password: String): Observable<User> {
        val user = UserWrapper(username, password)
        return Observable.create {
            val call = (dataSource as UserDataSource).signIn(user)
            callback(call, it)
        }
    }

}