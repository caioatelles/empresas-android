package com.example.appservice.repository

import android.util.Log
import com.example.appservice.BuildConfig
import com.example.appservice.datasource.DataSource
import com.microsoft.appcenter.crashes.Crashes
import okhttp3.Headers
import retrofit2.Call
import retrofit2.http.Header
import rx.Subscriber

abstract class Repository<T>(protected open val dataSource : DataSource)
{
    protected fun <T> callback(
        callResponse: Call<T>,
        c: Subscriber<in T>
    ) {
        val response = callResponse.execute()

        if (response.isSuccessful) {
            val dataResponse = response.body()

            c.onNext(dataResponse)
            c.onCompleted()
        } else {

            val t = Throwable(
                message =  response.errorBody()?.string(),
                cause = Throwable(response.code().toString())
            )

            if (BuildConfig.ANALYTICS) { Crashes.trackError(t) }

            c.onError(t)
        }
    }
}