package com.example.appservice.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Enterprises(
    val enterprises: List<Enterprise>
)

data class Enterprise(
    val city: String,
    val country: String,
    val description: String,
    val email_enterprise: String,
    @SerializedName("enterprise_name") val enterpriseName: String,
    @SerializedName("enterprise_type")  val enterpriseType: EnterpriseType,
    val facebook: String,
    val id: Int,
    val linkedin: String,
    @SerializedName("own_enterprise") val ownEnterprise: Boolean,
    val phone: String,
    val photo: String,
    val share_price: Double,
    val twitter: Any,
    val value: Int
): Serializable

data class EnterpriseType(
    @SerializedName("enterprise_type_name") val enterpriseTypeName: String,
    val id: Int
): Serializable