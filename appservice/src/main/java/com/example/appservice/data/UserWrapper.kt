package com.example.appservice.data

data class UserWrapper(
    val email: String,
    val password: String
)