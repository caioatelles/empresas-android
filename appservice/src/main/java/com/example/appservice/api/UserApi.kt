package com.example.appservice.api

import com.example.appservice.data.User
import com.example.appservice.data.UserWrapper
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface UserApi {
    @POST("users/auth/sign_in")
    fun signIn(@Body user: UserWrapper): Call<User>
}