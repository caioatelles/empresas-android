package com.example.appservice.api

import com.example.appservice.data.Enterprise
import com.example.appservice.data.Enterprises
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface EnterpriseApi {

    @GET("enterprises")
    fun getEnterprises(
        @Query("enterprise_types") typeId: Int? = null,
        @Query("name") name: String? = null
    ): Call<Enterprises>

    @GET("enterprises/{id}")
    fun get(@Path("id") id: Int): Call<Enterprise>

}