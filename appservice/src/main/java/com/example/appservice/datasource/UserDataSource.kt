package com.example.appservice.datasource

import com.example.appservice.api.UserApi
import com.example.appservice.data.UserWrapper

class UserDataSource: DataSource() {

    private lateinit var api: UserApi

    init { setApi() }

    private fun setApi() {
        api = retrofit.create(UserApi::class.java)
    }

    fun signIn(user: UserWrapper) = api.signIn(user)

}