package com.example.appservice.datasource

import com.example.appservice.api.EnterpriseApi

class EnterpriseDataSource : DataSource() {

    private lateinit var api: EnterpriseApi

    init {
        setApi()
    }

    private fun setApi() {
        api = retrofit.create(EnterpriseApi::class.java)
    }

    fun getEnterprises(typeId: Int?, name: String?) =
        api.getEnterprises(typeId, name)

    fun getEnterprise(id: Int) = api.get(id)

}