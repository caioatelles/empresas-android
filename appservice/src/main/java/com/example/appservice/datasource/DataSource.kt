package com.example.appservice.datasource

import android.util.Log
import com.example.appservice.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import java.util.logging.Logger

abstract class DataSource {

    companion object {
        lateinit var retrofit: Retrofit
        var accessToken: String = ""
        var uid: String = ""
        var client: String = ""
    }

    init { build() }

    private fun build() {

        val clientBuilder: OkHttpClient.Builder = OkHttpClient().newBuilder()
            .connectTimeout(15, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)

        clientBuilder.addInterceptor {

            val request = it.request()

            val builder = request
                .newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("access-token", accessToken)
                .addHeader("uid", uid)
                .addHeader("client", client)
                .build()

            val response = it.proceed(builder)

            if (accessToken == "") {
                response.headers["access-token"]?.let { accessToken = it }
                response.headers["uid"]?.let { uid = it }
                response.headers["client"]?.let { client = it }
            }

            response
        }

        val log = HttpLoggingInterceptor()
        log.setLevel(HttpLoggingInterceptor.Level.BODY)

        clientBuilder.addInterceptor(log)

        retrofit = Retrofit.Builder()
            .baseUrl("${BuildConfig.BASE_URL}api/${BuildConfig.API_VERSION}/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(clientBuilder.build())
            .build()
    }

}