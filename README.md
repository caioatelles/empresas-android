![N|Solid](logo_ioasys.png)

# README #

Estes documento README tem como objetivo fornecer as informações necessárias para compreendimento do que foi realizado no desenvolvimento do app Empresas.

### User Interface(UI) ###

* Zeplin (http://zeplin.io)
Login - teste_ioasys
Senha - ioasys123

### Tecnologias utilizadas ###

- Android Studio
- Kotlin
- Material Design
- API Ioasys
- MVVM
- Crashlytics do AppCenter

### Requisitos ###

Para executar os módulos do projeto é necessário ter os seguintes requisitos instalados no sistema:

- Java 11.x ou superior
- Android Studio
- O dispositivo ou emulador a rodar a aplicação deve ser de versão mínima 7.1.1 (Nougat)
- Conta no AppCenter para download da APK

### Dependências utilizadas ###

* Android libs: As bibliotecas padrão do androidX possibilitam a utilização das tecnologias mais recentes do padrão sugerido pela Google e do Android Jetpack;
* Retrofit e Okhttp: o retrofit é hoje a biblioteca de consumo de API's mais popular do desenvolvimento android, pois possibilita uma estruturação mais organizada e componetização das requisições realizadas pela aplicação. Tal comportamento pode ser visto na modularização da parte de comunicação com a api através do módulo "appservice";
* Koin: Biblioteca de injeção de dependências. Voltada para o conceito de clean code e padrão de projeto, o Koin é provavelmente a ferramenta de injeção de dependências mais enxuta que há para android. Não é tão robusta quanto o Hilt mas cumpre seu papel.
* RX: Bibliotecas de rxJava: possibilitam a implementação do comportamento de programação reativa, muito importante na arquitetura MVVM
* Room: Biblioteca de banco de dados do Android. Aqui entraria um TODO se houvesse mais tempo. Armazenaria todas as empresas numa tabela para funcionamento da aplicação offline.
* Glide: Biblioteca de download de imagens, facilita a requisição do download de imagens e sua injeção nos respectivos componentes. Reduz bastante código boilerplate, além do cache de imagem.

### TODO se houvesse mais tempo ###

* Alguns pequenos detalhes de layout não foram finalizados. Estes mais voltados para os campos de input de texto.
* Adição do módulo de Room para funcionamento da aplicação offline.
* Implementação dos testes unitários.
* Reatoração da classe LoadController, pois a mesma foi implementada por mim a algum tempo atrás e só foi aproveitada para o projeto. 

### Instalando a aplicação ###

#### Método 1

* Com uma conta do AppCenter disponível, solicitar acesso ao repositório do app no AppCenter enviando um e-mail com o assunto "Acesso de aplicação EmpresasIoasys" para csalestelles@gmail.com.
* Na sequência, basta acessar o link abaixo pelo dispositivo
https://install.appcenter.ms/users/cast-icomp.ufam.edu.br/apps/EnterprisesIoasys/releases/2
* Baixar a APK
* Abrir o gerenciador de arquivos do dispositivo no diretório em que a apk foi baixada e executá-la

#### Método 2
* Fazer o clone do repositório
* Abrir o Android Studio
* Importar o projeto
* Plugar o dispositivo via USB ao computador
* Após o dispositivo ser identificado pela IDE, apertar em "Run" ou o atalho "shift+F10" 

Made with :coffee: and ❤️ by Caio Telles.

